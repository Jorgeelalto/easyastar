# Imports used to:

# - Read the arguments passed from the terminal
from sys import argv
# - Time the execution
import time
# - Duplicate nodes
from copy import deepcopy


#   __________________________
#  /                          \
# <      PROBLEM DECODING      >
#  \__________________________/


###########
#   MAP   #
###########

# Now lets fill the map dictionary
map = {}

# Open the file
input = open(argv[1], 'r')
# Read the first line and get the stop list
stops = input.readline().lstrip().rstrip('\n').split()
# We put it in the map
map['stops'] = tuple(stops)

# Get the costs
costs = []
for i in stops:
    # Get the next line, it should be the one which has the costs
    # from each stop to the rest
    l = input.readline().lstrip().rstrip('\n').split()
    # Remove unused characters from the beginning
    l.pop(0)
    # And put this into costs
    s = 0
    for j in stops:
        # Create a temp structure with the style we want our result as
        t = [i, j, l[s]]
        # If cost is null, then put a -1 (instead of oo)
        if t[2] == '--':
            t[2] = -1
        else:
            t[2] = int(t[2].lstrip(' ').rstrip(' '  ))
        # Now save that list as a tuple in costs
        costs.append(tuple(t))
        # And increment the counter
        s = s + 1
# Set the costs list as a tuple and put it into map
map['costs'] = tuple(costs)

# Now get the stops where each school is
schools = input.readline().lstrip().rstrip('\n').split('; ')
# It will have a dictionary structure
map['schools_per_stop'] = {}
# Do it as many times as schools are
i = 0
for c in schools:
    schools[i] = schools[i].split(': ')
    # Put in each stop the corresponding school
    map['schools_per_stop'][schools[i][1]] = schools[i][0]
    i = i + 1

# The next line will be the people in each stop and where they are headed,
# this can be used to generate the starting node
p_per_stop = input.readline().lstrip().rstrip('\n').split('; ')

# To finish the map creation, we put the bus space
bus = input.readline().lstrip('B: ').rstrip('\n').split()
map['bus_space'] = int(bus[1])


###################
#  STARTING NODE  #
###################

# Now with the starting node
starting_node = {}

# First, fill the data related to the state
# Put the stop where the bus is
starting_node['current_stop'] = bus[0]
# Get the people that is in each stop and where they are headed
for i in range(len(p_per_stop)):
    p_per_stop[i] = p_per_stop[i].split(': ')
    for j in range(len(p_per_stop[i])):
        p_per_stop[i][j] = p_per_stop[i][j].split(', ')
        for k in range(len(p_per_stop[i][j])):
            p_per_stop[i][j][k] = p_per_stop[i][j][k].split()
    p_per_stop[i][0] = p_per_stop[i][0][0][0]
# Now we can put this data into the starting node
starting_node['people_per_stop'] = {}
for stop in p_per_stop:
    starting_node['people_per_stop'][stop[0]] = []
    for school in stop[1]:
        for n in range(int(school[0])):
            starting_node['people_per_stop'][stop[0]].append(school[1])
# Finally, the bus content
starting_node['bus_content'] = []

# Fill the rest of the empty fields
starting_node['cost'] = 0
starting_node['heuristic'] = 0
starting_node['result_of'] = 'starting'
starting_node['parent_node'] = None
# And some statistics
starting_node['visited_nodes'] = 0


# Print the content of both structures to user-check whether they are correct or not
print('\n -+- STARTING NODE -+- \n')

print('Initial stop: ' + starting_node['current_stop'])
print('People per stop:')
for i in starting_node['people_per_stop'].keys():
    print(' - ' + i + ': ' + str(starting_node['people_per_stop'][i]))

print('\n -+-      MAP      -+- \n')

print('Bus space: ' + str(map['bus_space']))
print('Schools por stop:')
for i in map['schools_per_stop'].keys():
    print(' - ' + i + ': ' + map['schools_per_stop'][i])
print('Map stops: ' + str(map['stops']))
for i in map['stops']:
    print(' - Costs from ' + i + ' to:')
    for j in [(k[1], k[2]) for k in map['costs'] if k[0] == i and k[1] != k[0] and k[2] >= 0]:
        print('   . ' + j[0] + ': ' + str(j[1]))



#   _____________________________
#  /                             \
# <       ACTION DEFINITION       >
#  \_____________________________/


# Get on the people in the stop where the bus is (if there are any)
def get_on(node, map):
    # First, create a copy of the node. That will be modified instead of the original
    new_node = deepcopy(node)
    # Since the cost of this action is zero, there is no need to change it
    new_node['result_of'] = 'get_on'
    # While there are people that need to get on and there is space on the bus,
    while len(new_node['people_per_stop'][new_node['current_stop']]) > 0 \
        and len(new_node['bus_content']) < map['bus_space']:
        # move the person from the stop to the bus
        i = new_node['people_per_stop'][new_node['current_stop']].pop()
        new_node['bus_content'].append(i)
        new_node['result_of'] += ' ' + i
    # Before finishing, remove empty lists (node maintenance)
    if not new_node['people_per_stop'][new_node['current_stop']]:
        new_node['people_per_stop'].pop(new_node['current_stop'])
    return new_node

# Get off the people that want to go to the school where the bus is (if there is any)
def get_off(node, map):
    # First, create a copy of the node. That will be modified instead of the original
    new_node = deepcopy(node)
    # Since the cost of this action is zero, there is no need to change it
    new_node['result_of'] = 'get_off'
    still_onboard = True
    # Check if there is anyone who can get off
    while still_onboard:
        still_onboard = False
        for i in new_node['bus_content']:
            if i == map['schools_per_stop'][node['current_stop']]:
                new_node['bus_content'].remove(i)
                new_node['result_of'] += ' ' + i
                still_onboard = True
    return new_node

# Move the bus to a new position
def move(node, map, new_position, cost):
    new_node = deepcopy(node)
    # In this case, there is a cost associated with the movement
    new_node['cost'] += cost
    # Change the position
    new_node['result_of'] = 'move ' + new_node['current_stop'] + ' ' + new_position
    new_node['current_stop'] = new_position
    return new_node



#   _______________________
#  /                       \
# <     OTHER FUNCTIONS     >
#  \_______________________/


# Expands a node, returning a list of successors
def expand(node, map):
    expanded_nodes = []
    # Check if there is people that can get on the bus (if there is space, and if there
    # is people that want to do it)
    if len(node['bus_content']) < map['bus_space'] \
        and node['current_stop'] in node['people_per_stop'].keys():
        expanded_nodes.append(get_on(node, map))
    # Check if there is people that can get off in this stop
    # There should be someone in the bus, and there should be a school here
    if node['bus_content'] \
        and node['current_stop'] in map['schools_per_stop'].keys():
        # Now, there has to be people that want to get off here
        for i in node['bus_content']:
            if i == map['schools_per_stop'][node['current_stop']]:
                expanded_nodes.append(get_off(node, map))
                # If there is at least one, that is enough (no need to check for more)
                break
    # Now we check the stops where it can move
    expanded_nodes.extend([move(node, map, i[1], i[2]) for i in map['costs'] if i[0] == node['current_stop'] and i[1] != i[0] and i[2] >= 0])
    # For each expanded node, we set his parent and heuristic
    for e in expanded_nodes:
        e['parent_node'] = node
        e['heuristic'] = heuristic(e, map)
    if not expanded_nodes:
        print('There are no nodes to expand')
    return expanded_nodes


# Checks whether the node is final
def is_it_final(node):
    # The bus must be empty and there should be no people waiting to be picked up
    return not node['bus_content'] and not node['people_per_stop']



#   ______________________
#  /                      \
# <       HEURISTICS       >
#  \______________________/


# Heuristic selector
def heuristic(node, map):
    if int(argv[2]) == 1:
        # Heuristic 1
        return heuristic1(node, map)
    else:
        print('Heuristic ' + argv[2] + ' does not exist! Using h(n) = 0')
        return 0


def heuristic1(node, map):
    # Relaxes the restriction of the cost of changing stops
    # In this case, counts the people to be delivered in the node and the people
    # who are still in the bus
    h = 0
    # Adds double the number of people still to be picked up
    for i in node['people_per_stop']:
        h += len(i) * 2
    # Adds the number of people in the bus if there is still people to be picked up
    if h > 0 and node['current_stop'] in map['schools_per_stop'].keys():
        h += len(node['bus_content'])
    return h



#            _____
#           /     \      /\
#          /   _   \   <    >
#         /   / \   \    \/
#        /   /   \   \
#       /   /_____\   \
#      /               \
#     /    _________    \
#    /    /         \    \
#   /____/           \____\


def astar(starting_node, map):
    # Reset some statistics
    expanded_nodes = 0
    visited_nodes = 0
    # Create the opened and closed node lists
    opened = []
    closed = []
    # Put the starting node into opened
    opened.append(starting_node)
    # While there are nodes in opened:
    while opened:
        # Get the first node from opened and put it in closed
        node = opened.pop()
        closed.append(node)
        # Check if it is a final state
        if is_it_final(node):
            break
        # If it is not, , expand it...
        new_nodes = expand(node, map)
        # (Increment the statistic of expanded nodes)
        expanded_nodes += len(new_nodes)
        # ...and put the children in opened if they are not in closed
        for n in new_nodes:
            if n not in closed:
                opened.append(n)
        # Sort opened by costs + heuristic
        opened.sort(key = lambda n: n['cost'] + n['heuristic'], reverse=True) 
    # When finished, return the list with the solution path
    solution = []
    # Put the last node of the path
    solution.append(node)
    # And from it, get iteratively the parent (until it gets to the starting node)
    for n in solution:
        if n['parent_node'] is not None:
            solution.append(n['parent_node'])
            # Also, calculate the number of visited nodes here
            if 'move' in n['result_of']:
                visited_nodes += 1
    # Return the inverse list (from start to finish)
    solution.reverse()
    solution[0]['expanded_nodes'] = expanded_nodes
    solution[0]['visited_nodes'] = visited_nodes
    return solution



#   _____________________
#  /                     \
# <       EXECUTION       >
#  \_____________________/


# Calculate the execution time, too
starting_time = time.time()
s = astar(starting_node, map)
total_time = time.time() - starting_time



#   ______________________
#  /                      \
# <       FILE WRITE       >
#  \______________________/


# Save the statistics into a file
stats_file = argv[1] + '.statistics'
with open(stats_file, 'w') as f:
    f.write('Time (in seconds): ' + str(total_time) + '\n')
    f.write('Total cost: ' + str(s[-1]['cost']) + '\n')
    f.write('Visited nodes: ' + str(s[0]['visited_nodes']) + '\n')
    f.write('Expanded nodes: ' + str(s[0]['expanded_nodes']) + '\n')

sol_string = ''
# Add the starting stop
sol_string = s[0]['current_stop']
for i in s:
    if 'move' in i['result_of']:
        # Put the state stop
        sol_string += ' -> ' + i['current_stop']
    # Check if people got on or got off
    if 'get_off' in i['result_of']:
        sol_string += ' (B:'
        # Get the people that got off in this action
        temp = i['result_of'].split()
        temp.remove('get_off')
        for t in set(temp):
            sol_string += ' ' + str(temp.count(t)) + ' ' + t
        sol_string += ')'
    if 'get_on' in i['result_of']:
        sol_string += ' (S:'
        # Get the people that got on in this action
        temp = i['result_of'].split()
        temp.remove('get_on')
        for t in set(temp):
            sol_string += ' ' + str(temp.count(t)) + ' ' + t
        sol_string += ')'


# Save the solution into a file
output_file = argv[1] + '.output'
with open(output_file, 'w') as f:
    f.write(sol_string + '\n')


# Print the solution to the screen
print('\n\n -+-    SOLUTION    -+- \n')
print(sol_string)
