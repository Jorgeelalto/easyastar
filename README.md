## What is this?

A simple A* implementation in Python. This was a school project, I fixed it a
bit so it would look nicer and work as a reference A* implementation for the
future (mainly for me).

## What does it do?

It solves a very specific transport type of problem. With a custom input file
format, and several output files originally specified as requirements.

* There is a map, with several stops. These stops may or may not be connected,
and have an associated cost (traveling from stop X to stop Y has a cost of C).
* There are schools, distributed among the stops (there are stops with a school,
and stops with no schools).
* There are people who want to go to those schools, distributed among the stops
too. There can be zero, one or more people in each stop, each one wanting to go
to the same or different schools.
* There is a bus, that can travel from and to these stops (affected by the
previously mentioned costs), which has a maximum number of people it can carry.
In each stop, people can pickup people, drop off people, or do nothing.

This solver (supposedly) gets the optimal path for the bus to take so the cost
of the route is minimum and all people gets to their school.

#### Data structures

You can check the node and map data structures in an example in
[doc/structures.py](doc/structures.py). Also, you can check an example of a
input problem file in [problems/problem.prob](problems/problem.prob).

## How do you execute it?

Just run `./astar.sh <problem> <heuristic>`, where:
* the first argument is the input problem file.
* The second argument is the heuristic function you want to use in the
execution. I only have one in this case. If you input an incorrect number, then
`h(n) = 0` will be used.

For example, `./astar.sh problem.prob 1` runs with the default data provided in
this repo.

## Does it solve the problem correctly?

Well, if we do not want the bus to get back to the starting position, then yes.
The route is considered as finished when the bus is empty and there is no more
people waiting to get on, so the finishing stop of the problem is unknown.

## How efficient is the solver?

I don't really know, probably not much. The implementation is made as readable
as possible, not much effort was applied into data structure or procedure
optimization. But it works, and easily solves problems with a readable size, so
I consider it a success: I can understand how A* works, and I can check whether
the problem is solved correctly or not.