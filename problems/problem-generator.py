from sys import argv
from random import randint, choice


if not len(argv) == 2:
    print('Te falta el argumento del numero de paradas!')
    exit(-1)

with open('problema_gen.prob', 'w') as f:
    paradas = ['P' + str(i) for i in range(int(argv[1]))]
    # La primera linea es nada mas que las paradas que hay en total
    f.write('  ')
    for p in paradas:
        f.write(' ' + p)
    # Luego vamos linea por linea de cada parada
    m = []
    for p in range(len(paradas)):
        m.append([])
        for q in range(len(paradas)):
            # 33% de posibilidades de que haya un camino de p a q
            if randint(0, 3) == 0:
                m[p].append(str(int(((randint(0, 100) / 100) ** 1.5) * 16 + 2)))
            else:
                m[p].append('--')
    # Aplicamos (o no) simetria
    for p in range(len(paradas)):
        for q in range(len(paradas)):
            # 33% de posibilidades de aplicar simetria
            if randint(0, 2) == 1:
                m[p][q] = m[q][p]
            # 33% de posibilidades de aplicar simetria inversa
            if randint(0, 2) == 2:
                m[q][p] = m[p][q]
            # Los bucles los quitamos
            if p == q:
                m[p][q] = '--'
    # Luego vamos linea por linea escribiendo todo
    for p in range(len(paradas)):
        f.write('\n')
        f.write(paradas[p])
        for q in range(len(paradas)):
            f.write(' ')
            f.write(m[p][q])


    # Ahora escribimos los colegios que hay. Aproximadamente pondremos
    # tantos colegios como la raiz cuadrada del numero de paradas
    f.write('\n')
    n_colegios = int(len(paradas) ** 0.5)
    # Y tantas paradas con ninyos como la mitad  del numero de paradas
    n_paradas_con_ninyos = int(len(paradas) * 0.5)
    paradas_colegios = []
    paradas_ninyos  = []
    # En dos listas, cada una un tipo de paradas anteriores, metemos
    # aleatoriamente las paradas disponibles
    for p in paradas:
        if randint(0, 1):
            paradas_colegios.append(p)
        else:
            paradas_ninyos.append(p)
    # Ahora ajustamos los numeros de paradas
    while n_colegios < len(paradas_colegios):
        paradas_colegios.pop(randint(0, len(paradas_colegios) - 1))        
    while n_paradas_con_ninyos < len(paradas_ninyos):
        paradas_ninyos.pop(randint(0, len(paradas_ninyos) - 1))

    # Escribimos los colegios
    for i in range(n_colegios):
        f.write('C' + str(i) + ': ')
        f.write(paradas_colegios.pop(randint(0, len(paradas_colegios) - 1)))
        if i < (n_colegios - 1):
            f.write('; ')

    # Y ahora escribimos los ninyos en las paradas
    f.write('\n')
    for p in range(len(paradas_ninyos)):
        f.write('P' + str(p) + ':')
        escrito = False
        for c in range(n_colegios):
            if randint(0, 2) > 0:
                f.write(' ' + str(randint(1, int((n_paradas_con_ninyos * 2) / n_colegios))))
                f.write(' C' + str(c))
                escrito = True
        if not escrito:
            f.write(' ' + str(randint(1, int((n_paradas_con_ninyos * 2) / n_colegios))))
            f.write(' C' + str(c))
            escrito = True
        if p < (n_paradas_con_ninyos - 1):
            f.write('; ')

    # Finalmente, los datos del bus
    f.write('\n')
    f.write('B: ' + choice(paradas) +  ' ' + str(randint(5, 5 + int(len(paradas) ** 0.5))))
    f.write('\n')
