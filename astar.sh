# Check the arguments
if [ $# -ne 2 ]; then
    echo "Number of arguments not valid"
    echo "Usage: ./astar.sh <problem> <heuristic>"
    echo "- <problem> should be a correctly formatted .prob file"
    echo "- <heuristic> should be the number of your desired heuristic."
    echo "  If it does not exist, it will default to h(n) = 0"
    exit 1
fi

echo "/!\\ Needs Python 3, does not work under Python 2 /!\\"
# Check that the Python version is the one we need
version=$(python -V 2>&1 | cut -b 8)
if [[ $version -eq '3' ]]; then
    echo "Running: python astar.py $1 $2"
    python astar.py $1 $2
else
    echo "The default version is not 3.X."
    echo "Trying to run: python3 astar.py $1 $2"
    python3 astar.py $1 $2
fi
